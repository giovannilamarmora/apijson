package com.be.digitech.exercise.credential.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.be.digitech.exercise.credential.dao.UserCredentialDao;
import com.be.digitech.exercise.credential.dto.TokenDTO;
import com.be.digitech.exercise.credential.dto.UserCredentialDTO;
import com.be.digitech.exercise.credential.dto.UserDTO;
import com.be.digitech.exercise.credential.entity.UserCredentialEntity;
import com.be.digitech.exercise.credential.exception.CredentialException;

@SpringBootTest
public class UserCredentialServiceTest {

	private static final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	private static final String USER_USERNAME = "my-user-username";
	private static final String USER_PASSWORD = "my-user-password";
	private static final String USER_PASSWORD_WRONG = "the-user-wrong-password";
	private static final String USER_JWT = "the-user-jwt-token";
	private static final String USER_JWT_WRONG = "the-user-jwt-wrong";
	private static final String USER_PASS_HASHED = bCryptPasswordEncoder.encode(USER_PASSWORD);
	private static final TokenDTO USER_TOKEN = new TokenDTO(USER_JWT);
	private static final UserCredentialDTO USER_USER_CREDENTIAL_DTO = new UserCredentialDTO(USER_USERNAME,
			USER_PASSWORD);
	private static final UserCredentialEntity USER_USER_CREDENTIAL_ENTITY = new UserCredentialEntity(0, USER_USERNAME,
			USER_PASS_HASHED, UserCredentialEntity.USER_ROLE);
	private static final UserDTO USER_USER_DTO = new UserDTO(USER_USERNAME, UserCredentialEntity.USER_ROLE);

	private static final String ADMIN_USERNAME = "my-admin-username";
	private static final String ADMIN_PASSWORD = "my-admin-password";
	private static final String ADMIN_JWT = "the-admin-jwt-token";
	private static final String ADMIN_PASS_HASHED = bCryptPasswordEncoder.encode(ADMIN_PASSWORD);
	private static final UserDTO ADMIN_USER = new UserDTO(ADMIN_USERNAME, UserCredentialEntity.ADMIN_ROLE);
	private static final TokenDTO ADMIN_TOKEN = new TokenDTO(ADMIN_JWT);
	private static final UserCredentialEntity ADMIN_USER_CREDENTIAL_ENTITY = new UserCredentialEntity(0, ADMIN_USERNAME,
			ADMIN_PASS_HASHED, UserCredentialEntity.ADMIN_ROLE);

	@Mock
	UserCredentialDao dao;

	@InjectMocks
	UserCredentialService service;

	@Mock
	TokenService tokenService;

	@Captor
	ArgumentCaptor<UserCredentialDTO> userCredentialDTOCaptor;

	@Captor
	ArgumentCaptor<TokenDTO> tokenDTOCaptor;

	@Captor
	ArgumentCaptor<UserDTO> userDTOCaptor;

	@BeforeEach
	void setPassword_setDefaultPassword() {
		USER_USER_CREDENTIAL_DTO.setPassword(USER_PASSWORD);
	}

	@Test
	void signUpUser_shouldInsertUserCorrectly() throws Exception {
		doNothing().when(dao).insertUserCredential(userCredentialDTOCaptor.capture());

		service.signUp(USER_USER_CREDENTIAL_DTO);

		assertTrue(bCryptPasswordEncoder.matches(USER_PASSWORD, userCredentialDTOCaptor.getValue().getPassword()));
		assertEquals(USER_USER_CREDENTIAL_DTO.getUsername(), userCredentialDTOCaptor.getValue().getUsername());
	}

	@Test
	void signUpUser_shouldReturnInvalidInputOnPasswordNull() throws Exception {
		UserCredentialDTO user = new UserCredentialDTO(USER_USERNAME, null);

		CredentialException expected = new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION);
		CredentialException actual = assertThrows(CredentialException.class, () -> service.signUp(user));

		assertEquals(expected.getCode(), actual.getCode());
	}

	@Test
	void login_shouldReturnTokenCorrectly() throws Exception {
		when(dao.getCredential(USER_USER_CREDENTIAL_DTO)).thenReturn(USER_USER_CREDENTIAL_ENTITY);
		when(tokenService.generateToken(userDTOCaptor.capture())).thenReturn(USER_TOKEN);

		TokenDTO actual = service.login(USER_USER_CREDENTIAL_DTO);

		assertEquals(USER_TOKEN.getToken(), actual.getToken());
	}

	@Test
	void login_shouldThrowWrongCredential() throws Exception {
		when(dao.getCredential(USER_USER_CREDENTIAL_DTO)).thenReturn(null);

		CredentialException expected = new CredentialException(CredentialException.Code.WRONG_CREDENTIAL);
		CredentialException actual = assertThrows(CredentialException.class,
				() -> service.login(USER_USER_CREDENTIAL_DTO));

		assertEquals(expected.getCode(), actual.getCode());
	}

	@Test
	void login_shouldThrowInvalidInputOnPasswordNull() throws Exception {
		UserCredentialDTO user = new UserCredentialDTO(USER_USERNAME, null);

		CredentialException expected = new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION);
		CredentialException actual = assertThrows(CredentialException.class, () -> service.login(user));

		assertEquals(expected.getCode(), actual.getCode());
	}

	@Test
	void login_shouldThrowWrongPassword() throws Exception {
		UserCredentialDTO user = new UserCredentialDTO(USER_USERNAME, USER_PASSWORD_WRONG);

		when(dao.getCredential(user)).thenReturn(USER_USER_CREDENTIAL_ENTITY);

		CredentialException expected = new CredentialException(CredentialException.Code.WRONG_CREDENTIAL);
		CredentialException actual = assertThrows(CredentialException.class, () -> service.login(user));

		assertEquals(expected.getCode(), actual.getCode());
	}

	@Test
	void getUser_shouldReturnCorrectUser() throws Exception {
		when(tokenService.parseToken(USER_TOKEN)).thenReturn(USER_USER_DTO);

		UserDTO actual = service.getUser(USER_TOKEN);

		assertEquals(USER_USER_DTO.getUsername(), actual.getUsername());
		assertEquals(USER_USER_DTO.getRole(), actual.getRole());
	}

	@Test
	void getUser_shouldReturnInvalidInputOnTokenNull() throws Exception {
		TokenDTO token = new TokenDTO(null);

		CredentialException expected = new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION);
		CredentialException actual = assertThrows(CredentialException.class, () -> service.getUser(token));

		assertEquals(expected.getCode(), actual.getCode());
	}

	@Test
	void getUser_shouldReturnUnauthorizedOnInvalidToken() throws Exception {
		TokenDTO token = new TokenDTO(USER_JWT_WRONG);

		when(tokenService.parseToken(token)).thenThrow(new CredentialException(CredentialException.Code.UNAUTHORIZED));

		CredentialException expected = new CredentialException(CredentialException.Code.UNAUTHORIZED);
		CredentialException actual = assertThrows(CredentialException.class, () -> service.getUser(token));

		assertEquals(expected.getCode(), actual.getCode());
	}

	@Test
	void getUsers_adminshouldReiceveListOfUsers() throws Exception {
		List<UserCredentialEntity> listUsers = List.of(ADMIN_USER_CREDENTIAL_ENTITY, USER_USER_CREDENTIAL_ENTITY);
		when(tokenService.parseToken(ADMIN_TOKEN)).thenReturn(ADMIN_USER);
		when(dao.getUsers()).thenReturn(listUsers);

		List<UserDTO> actuals = service.getUsers(ADMIN_TOKEN);

		assertEquals(listUsers.size(), actuals.size());
		for (int i = 0; i < actuals.size(); i++) {
			UserDTO actual = actuals.get(i);
			UserCredentialEntity expected = listUsers.get(i);

			assertEquals(expected.getUsername(), actual.getUsername());
			assertEquals(expected.getRole(), actual.getRole());
		}
	}

	@Test
	void getUsers_adminShouldThrowInvalidInput() throws Exception {
		TokenDTO token = new TokenDTO(null);

		when(tokenService.parseToken(token))
				.thenThrow(new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION));

		CredentialException expected = new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION);
		CredentialException actual = assertThrows(CredentialException.class, () -> service.getUsers(token));

		assertEquals(expected.getCode(), actual.getCode());
	}

	@Test
	void getUsers_adminShouldThrowIfTheUserIsNotAnAdmin() throws Exception {
		when(tokenService.parseToken(USER_TOKEN)).thenReturn(USER_USER_DTO);

		CredentialException expected = new CredentialException(CredentialException.Code.NOT_ALLOWED);
		CredentialException actual = assertThrows(CredentialException.class, () -> service.getUsers(USER_TOKEN));

		assertEquals(expected.getCode(), actual.getCode());
	}

}
