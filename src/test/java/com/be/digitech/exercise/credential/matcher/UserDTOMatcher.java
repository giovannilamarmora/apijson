package com.be.digitech.exercise.credential.matcher;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import com.be.digitech.exercise.credential.dto.UserDTO;

public class UserDTOMatcher extends BaseMatcher<UserDTO> {

	final UserDTO expected;

	public UserDTOMatcher(UserDTO expected) {
		super();
		this.expected = expected;
	}

	@Override
	public boolean matches(Object actual) {
		final UserDTO res = (UserDTO) actual;
		return expected.getUsername() == res.getUsername();
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("Response should be ").appendValue(expected);

	}

}
