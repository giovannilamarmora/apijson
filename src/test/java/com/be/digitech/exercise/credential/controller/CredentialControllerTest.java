package com.be.digitech.exercise.credential.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.be.digitech.exercise.credential.dto.TokenDTO;
import com.be.digitech.exercise.credential.dto.UserCredentialDTO;
import com.be.digitech.exercise.credential.dto.UserDTO;
import com.be.digitech.exercise.credential.entity.UserCredentialEntity;
import com.be.digitech.exercise.credential.exception.CredentialException;
import com.be.digitech.exercise.credential.service.UserCredentialService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = UserCredentialController.class)
public class CredentialControllerTest {

	private static final String WRONG_PASSWORD = "giova";
	private static final String USER_USERNAME = "my-user-username";
	private static final String USER_PASSWORD = "my-user-password";
	private static final String USER_JWT = "my-user-jwt-token";
	private static final String USER_JWT_WRONG = "my-user-jwt-token-wrong";

	private static final UserCredentialDTO USER_USER_CREDENTIAL_DTO = new UserCredentialDTO(USER_USERNAME,
			USER_PASSWORD);
	private static final UserDTO USER_USER_DTO = new UserDTO(USER_USERNAME, UserCredentialEntity.USER_ROLE);
	private static final TokenDTO USER_TOKEN_JWT = new TokenDTO(USER_JWT);

	@MockBean
	private UserCredentialService credential;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void addUser_shouldReturnStatus200() throws Exception {
		String userAsString = objectMapper.writeValueAsString(USER_USER_CREDENTIAL_DTO);

		Mockito.doNothing().when(credential).signUp(USER_USER_CREDENTIAL_DTO);
		mockMvc.perform(post("/credential/signup").contentType(MediaType.APPLICATION_JSON).content(userAsString))
				.andExpect(status().isOk());
	}

	@Test
	void addUser_shouldReturnError400OnBadInput() throws Exception {
		Mockito.doThrow(new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION)).when(credential)
				.signUp(Mockito.any());
		mockMvc.perform(post("/credential/signup").contentType(MediaType.APPLICATION_JSON)
				.content("{\"user\": 30, \"password\": 90}")).andExpect(status().isBadRequest());
	}

	@Test
	void login_shouldReturnTokenCorrectly() throws Exception {
		String userAsString = objectMapper.writeValueAsString(USER_USER_CREDENTIAL_DTO);
		String tokenAsString = objectMapper.writeValueAsString(USER_TOKEN_JWT);

		Mockito.doReturn(USER_TOKEN_JWT).when(credential).login(Mockito.any());
		mockMvc.perform(post("/credential/login").contentType(MediaType.APPLICATION_JSON).content(userAsString))
				.andExpect(status().isOk()).andExpect(content().json(tokenAsString));
	}

	@Test
	void login_shouldReturnError400OnBadInput() throws Exception {
		Mockito.doThrow(new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION)).when(credential)
				.login(Mockito.any());
		mockMvc.perform(post("/credential/login").contentType(MediaType.APPLICATION_JSON)
				.content("{\"user1\": \"giovanni\", \"password\": 90}")).andExpect(status().isBadRequest());
	}

	@Test
	void login_shouldReturnWrongcredential() throws Exception {
		UserCredentialDTO user = new UserCredentialDTO(USER_USERNAME, WRONG_PASSWORD);
		String userAsString = objectMapper.writeValueAsString(user);

		Mockito.doThrow(new CredentialException(CredentialException.Code.WRONG_CREDENTIAL)).when(credential)
				.login(Mockito.any());
		mockMvc.perform(post("/credential/login").contentType(MediaType.APPLICATION_JSON).content(userAsString))
				.andExpect(status().is(401));
	}

	@Test
	void tokenUser_shouldReturnUser() throws Exception {
		String userAsString = objectMapper.writeValueAsString(USER_USER_DTO);

		Mockito.doReturn(USER_USER_DTO).when(credential).getUser(Mockito.any());
		mockMvc.perform(MockMvcRequestBuilders.get("/credential/token").header("Authorization", "Bearer " + USER_JWT))
				.andExpect(status().isOk()).andExpect(content().json(userAsString));
	}

	@Test
	void tokenUser_shouldReturnUnauthorized() throws Exception {
		TokenDTO token = new TokenDTO(USER_JWT_WRONG);

		Mockito.doThrow(new CredentialException(CredentialException.Code.UNAUTHORIZED)).when(credential)
				.getUser(Mockito.any());
		mockMvc.perform(
				MockMvcRequestBuilders.get("/credential/token").header("Authorization", "Bearer " + token.getToken()))
				.andExpect(status().is(401));
	}

	@Test
	void tokenUser_shouldReturnInvalidInput() throws Exception {
		TokenDTO token = new TokenDTO(null);

		Mockito.doThrow(new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION)).when(credential)
				.getUser(token);
		mockMvc.perform(MockMvcRequestBuilders.get("/credential/token")).andExpect(status().isBadRequest());
	}

	@Test
	void listUsers_adminShouldReceiveTheListCorrectly() throws Exception {
		List<UserDTO> listUsers = List.of(USER_USER_DTO, USER_USER_DTO);
		String usersAsString = objectMapper.writeValueAsString(listUsers);

		Mockito.doReturn(listUsers).when(credential).getUsers(Mockito.any());
		mockMvc.perform(MockMvcRequestBuilders.get("/credential/admin").header("Authorization", "Bearer " + USER_JWT))
				.andExpect(status().isOk()).andExpect(content().json(usersAsString));
	}

	@Test
	void listUsers_adminShouldReturnNotAllowed() throws Exception {
		TokenDTO token = new TokenDTO(USER_JWT_WRONG);

		Mockito.doThrow(new CredentialException(CredentialException.Code.NOT_ALLOWED)).when(credential)
				.getUsers(Mockito.any());
		mockMvc.perform(
				MockMvcRequestBuilders.get("/credential/admin").header("Authorization", "Bearer " + token.getToken()))
				.andExpect(status().isForbidden());
	}

	@Test
	void listUsers_adminShouldReturnInvalidInput() throws Exception {
		TokenDTO token = new TokenDTO(null);

		Mockito.doThrow(new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION)).when(credential)
				.getUsers(token);
		mockMvc.perform(MockMvcRequestBuilders.get("/credential/admin")).andExpect(status().isBadRequest());
	}

}
