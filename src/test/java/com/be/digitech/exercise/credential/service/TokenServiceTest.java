package com.be.digitech.exercise.credential.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import com.be.digitech.exercise.credential.dto.TokenDTO;
import com.be.digitech.exercise.credential.dto.UserDTO;
import com.be.digitech.exercise.credential.entity.UserCredentialEntity;
import com.be.digitech.exercise.credential.exception.CredentialException;

@SpringBootTest
public class TokenServiceTest {

	private static final String INVALID_TOKEN_JWT = "my-invalid-token-jwt";

	private static final String USERNAME = "my-username";

	@Value(value = "${jwt.secret}")
	private String secret;

	@Value(value = "${jwt.time}")
	private String expirationTime;

	@InjectMocks
	TokenService service;

	@BeforeEach
	void checkField() {
		ReflectionTestUtils.setField(service, "expirationTime", expirationTime);
		ReflectionTestUtils.setField(service, "secret", secret);
	}

	@Test
	void generateToken_parseToken_shouldReturnSameUser() throws Exception {
		UserDTO userDto = new UserDTO(USERNAME, UserCredentialEntity.USER_ROLE);

		TokenDTO token = service.generateToken(userDto);
		UserDTO parseUser = service.parseToken(token);

		assertEquals(userDto.getUsername(), parseUser.getUsername());
		assertEquals(userDto.getRole(), parseUser.getRole());
	}

	@Test
	void parseToken_shouldThrowUnauthorizedOnInvalidToken() throws Exception {
		TokenDTO token = new TokenDTO(INVALID_TOKEN_JWT);

		CredentialException expectedException = new CredentialException(CredentialException.Code.UNAUTHORIZED);
		CredentialException actualException = assertThrows(CredentialException.class, () -> service.parseToken(token));

		assertEquals(expectedException.getCode(), actualException.getCode());

	}

}
