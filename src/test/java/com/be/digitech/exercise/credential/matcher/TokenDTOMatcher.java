package com.be.digitech.exercise.credential.matcher;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import com.be.digitech.exercise.credential.dto.TokenDTO;

public class TokenDTOMatcher extends BaseMatcher<TokenDTO> {

	private TokenDTO expected;

	public TokenDTOMatcher(TokenDTO expected) {
		super();
		this.expected = expected;
	}

	@Override
	public boolean matches(Object actual) {
		final TokenDTO res = (TokenDTO) actual;
		return expected.getToken() == res.getToken();
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("Response should be ").appendValue(expected);

	}

}
