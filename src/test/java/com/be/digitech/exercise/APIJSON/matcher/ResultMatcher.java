package com.be.digitech.exercise.APIJSON.matcher;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import com.be.digitech.exercise.APIJSON.dto.Result;

public class ResultMatcher extends BaseMatcher<Result> {

	private Result expected;

	public ResultMatcher(Result expected) {
		super();
		this.expected = expected;
	}

	@Override
	public boolean matches(Object actual) {
		final Result res = (Result) actual;
		return expected.getResult() == res.getResult();
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("getNumber should be ").appendValue(expected);
	}
}
