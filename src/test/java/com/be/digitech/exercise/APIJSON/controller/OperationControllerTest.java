package com.be.digitech.exercise.APIJSON.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.be.digitech.exercise.APIJSON.dto.Result;
import com.be.digitech.exercise.APIJSON.exception.OperationException;
import com.be.digitech.exercise.APIJSON.service.OperationsService;

@WebMvcTest(OperationController.class)
public class OperationControllerTest {
	@MockBean
	private OperationsService operationsService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void sum_shouldReturn400OnBadInput() throws Exception {
		when(operationsService.sum(Mockito.any()))
				.thenThrow(new OperationException(OperationException.Code.INVALID_NUMBER));

		MockHttpServletRequestBuilder request = post("/operation/sum").contentType(MediaType.APPLICATION_JSON)
				.content("{\"creditCardNumber\": \"4532756279624064\"}");

		mockMvc.perform(request).andExpect(status().isBadRequest());
	}

	@Test
	void sum_shouldReturnTheResult() throws Exception {
		when(operationsService.sum(Mockito.any())).thenReturn(new Result(4));

		MockHttpServletRequestBuilder request = post("/operation/sum").contentType(MediaType.APPLICATION_JSON)
				.content("{\"number1\": 1, \"number2\": 1}");

		mockMvc.perform(request).andExpect(status().isOk()).andExpect(content().json("{\"result\":4}"));
	}

	@Test
	void sum_shouldReturnExceptionOnZeroNumbers() throws Exception {
		when(operationsService.sum(Mockito.any()))
				.thenThrow(new OperationException(OperationException.Code.ZERO_NUMBERS));

		MockHttpServletRequestBuilder request = post("/operation/sum").contentType(MediaType.APPLICATION_JSON)
				.content("{\"number1\": 0, \"number2\": 0}");

		mockMvc.perform(request).andExpect(status().isConflict());
	}
}
