package com.be.digitech.exercise.APIJSON.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.be.digitech.exercise.APIJSON.dto.Numbers;
import com.be.digitech.exercise.APIJSON.dto.Result;
import com.be.digitech.exercise.APIJSON.exception.OperationException;
import com.be.digitech.exercise.APIJSON.matcher.ResultMatcher;

public class OperationServiceTest {

	private final OperationsService operation = new OperationsService();

	@Test
	void sum_shouldSumPositiveNumberCorrectly() throws Exception {
		Result expected = new Result(3);
		Result actual = operation.sum(new Numbers(1, 2));
		assertThat(actual, new ResultMatcher(expected));
	}

	@Test
	void sum_shouldSumNegativeNumberCorrectly() throws Exception {
		Result expected = new Result(-3);
		Result actual = operation.sum(new Numbers(-1, -2));
		assertThat(actual, new ResultMatcher(expected));
	}

	@Test
	void sum_shouldReturnExceptionOnZeroNumbers() throws Exception {
		OperationException exception = assertThrows(OperationException.class, () -> {
			operation.sum(new Numbers(0, 0));
		});
		assertEquals(exception.getCode(), OperationException.Code.ZERO_NUMBERS);
	}

	@Test
	void sum_shouldSumZeroWithPositiveNumberCorrectly() throws Exception {
		Result expected = new Result(5);
		Result actual = operation.sum(new Numbers(0, 5));
		assertThat(actual, new ResultMatcher(expected));
	}

	@Test
	void sum_ZeroWithNegativeNumbersCorrectly() throws Exception {
		Result expected = new Result(-5);
		Result actual = operation.sum(new Numbers(0, -5));
		assertThat(actual, new ResultMatcher(expected));
	}

	@Test
	void sum_PositiveWithNegativeNumbersCorrectly() throws Exception {
		Result expected = new Result(-6);
		Result actual = operation.sum(new Numbers(4, -10));
		assertThat(actual, new ResultMatcher(expected));
	}
}
