package com.be.digitech.exercise.credential.entity;

import javax.validation.constraints.NotNull;

public class UserCredentialEntity {

	public static final String ADMIN_ROLE = "ADMIN";
	public static final String USER_ROLE = "USER";

	@NotNull
	private int id;
	@NotNull
	private String username;
	@NotNull
	private String password;
	@NotNull
	private String role;

	public UserCredentialEntity(@NotNull int id, @NotNull String username, @NotNull String password,
			@NotNull String role) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
