package com.be.digitech.exercise.credential.dto;

import javax.validation.constraints.NotNull;

public class TokenDTO {

	@NotNull
	private String token;

	public TokenDTO(@NotNull String token) {
		super();
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
