package com.be.digitech.exercise.credential.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.be.digitech.exercise.credential.dto.UserCredentialDTO;
import com.be.digitech.exercise.credential.entity.UserCredentialEntity;

@Repository
public class UserCredentialDao {

	private static final String SELECT_FROM_USERS_WHERE_ROLE = "SELECT * FROM users WHERE role = 'USER'";
	private static final String SELECT_FROM_USERS = "SELECT * FROM users WHERE username = ?";
	private static final String INSERT_INTO_USERS = "INSERT INTO users (username, password, role) VALUES (?, ?, 'USER')";
	private String dbAddress;
	private String username;
	private String password;

	public UserCredentialDao(@Value("${db.address}") String dbAddress, @Value("${db.username}") String username,
			@Value("${db.password}") String password) {
		super();
		this.dbAddress = dbAddress;
		this.username = username;
		this.password = password;
	}

	public void insertUserCredential(UserCredentialDTO user) {
		try {
			Connection connection = DriverManager.getConnection(dbAddress, username, password);
			String sqlCommand = INSERT_INTO_USERS;
			PreparedStatement pstm = connection.prepareStatement(sqlCommand);
			pstm.setString(1, user.getUsername());
			pstm.setString(2, user.getPassword());
			pstm.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public UserCredentialEntity getCredential(UserCredentialDTO user) {
		UserCredentialEntity userCredential = null;
		try {
			Connection connection = DriverManager.getConnection(dbAddress, username, password);
			String sqlCommand = SELECT_FROM_USERS;
			PreparedStatement pstm = connection.prepareStatement(sqlCommand);
			pstm.setString(1, user.getUsername());
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				userCredential = new UserCredentialEntity(rs.getInt("id"), rs.getString("username"),
						rs.getString("password"), rs.getString("role"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return userCredential;

	}

	public List<UserCredentialEntity> getUsers() {
		List<UserCredentialEntity> listUser = new ArrayList<UserCredentialEntity>();
		try {
			Connection connection = DriverManager.getConnection(dbAddress, username, password);
			String sqlCommand = SELECT_FROM_USERS_WHERE_ROLE;
			PreparedStatement pstm = connection.prepareStatement(sqlCommand);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				listUser.add(new UserCredentialEntity(rs.getInt("id"), rs.getString("username"),
						rs.getString("password"), rs.getString("role")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return listUser;
	}
}
