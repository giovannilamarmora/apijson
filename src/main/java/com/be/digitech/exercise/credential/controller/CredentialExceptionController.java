package com.be.digitech.exercise.credential.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.be.digitech.exercise.credential.dto.ErrorDTO;
import com.be.digitech.exercise.credential.exception.CredentialException;

@ControllerAdvice
public class CredentialExceptionController {

	@ExceptionHandler(value = CredentialException.class)
	public ResponseEntity<ErrorDTO> exception(CredentialException e) {
		ErrorDTO error = new ErrorDTO(null);
		e.printStackTrace();
		switch (e.getCode()) {
		case INVALID_INPUT_EXCEPTION:
			error.setResponse("Error for Bad input Exception");
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		case WRONG_CREDENTIAL:
			error.setResponse("Wrong Credential");
			return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
		case UNAUTHORIZED:
			error.setResponse("Error 401");
			return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
		case NOT_ALLOWED:
			error.setResponse("Not Allowed User");
			return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
		default:
			error.setResponse("Internal Server Error");
			return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
