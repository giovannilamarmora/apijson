package com.be.digitech.exercise.credential.dto;

public class ErrorDTO {

	private String response;

	public ErrorDTO(String response) {
		super();
		this.response = response;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
