package com.be.digitech.exercise.credential.exception;

public class CredentialException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2043177912422833687L;
	private final Code code;

	public static enum Code {
		INVALID_INPUT_EXCEPTION, WRONG_CREDENTIAL, UNAUTHORIZED, NOT_ALLOWED;
	}

	public CredentialException(Code code) {
		super();
		this.code = code;
	}

	public Code getCode() {
		return code;
	}

}
