package com.be.digitech.exercise.credential.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.be.digitech.exercise.credential.dao.UserCredentialDao;
import com.be.digitech.exercise.credential.dto.TokenDTO;
import com.be.digitech.exercise.credential.dto.UserCredentialDTO;
import com.be.digitech.exercise.credential.dto.UserDTO;
import com.be.digitech.exercise.credential.entity.UserCredentialEntity;
import com.be.digitech.exercise.credential.exception.CredentialException;

@Service
public class UserCredentialService {

	@Autowired
	private UserCredentialDao dao;

	@Autowired
	private TokenService tokenService;

	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	public void signUp(UserCredentialDTO userCredential) throws CredentialException {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<UserCredentialDTO>> violations = validator.validate(userCredential);
		if (!violations.isEmpty()) {
			throw new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION);
		}
		userCredential.setPassword(bCryptPasswordEncoder.encode(userCredential.getPassword()));
		dao.insertUserCredential(userCredential);
	}

	public TokenDTO login(UserCredentialDTO userCredential) throws CredentialException {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<UserCredentialDTO>> violations = validator.validate(userCredential);
		if (!violations.isEmpty()) {
			throw new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION);
		}
		UserCredentialEntity userEntity = dao.getCredential(userCredential);
		if (userEntity == null) {
			throw new CredentialException(CredentialException.Code.WRONG_CREDENTIAL);
		}
		boolean matches = bCryptPasswordEncoder.matches(userCredential.getPassword(), userEntity.getPassword());
		if (!matches) {
			throw new CredentialException(CredentialException.Code.WRONG_CREDENTIAL);
		}
		UserDTO userDto = new UserDTO(userEntity.getUsername(), userEntity.getRole());
		TokenDTO token = tokenService.generateToken(userDto);
		return token;
	}

	public UserDTO getUser(TokenDTO token) throws CredentialException {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<TokenDTO>> violations = validator.validate(token);
		if (!violations.isEmpty()) {
			throw new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION);
		}
		UserDTO user = tokenService.parseToken(token);

		return user;
	}

	public List<UserDTO> getUsers(TokenDTO token) throws CredentialException {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<TokenDTO>> violations = validator.validate(token);
		if (!violations.isEmpty()) {
			throw new CredentialException(CredentialException.Code.INVALID_INPUT_EXCEPTION);
		}
		UserDTO user = tokenService.parseToken(token);
		if (!user.getRole().equalsIgnoreCase(UserCredentialEntity.ADMIN_ROLE)) {
			throw new CredentialException(CredentialException.Code.NOT_ALLOWED);
		}
		List<UserDTO> listUsers = new ArrayList<>();
		List<UserCredentialEntity> list = dao.getUsers();
		for (int i = 0; i < list.size(); i++) {
			listUsers.add(new UserDTO(list.get(i).getUsername(), list.get(i).getRole()));
		}
		return listUsers;
	}
}
