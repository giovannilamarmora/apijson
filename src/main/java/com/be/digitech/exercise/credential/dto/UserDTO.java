package com.be.digitech.exercise.credential.dto;

import javax.validation.constraints.NotNull;

public class UserDTO {

	@NotNull
	private String username;

	@NotNull
	private String role;

	public UserDTO(@NotNull String username, @NotNull String role) {
		super();
		this.username = username;
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
