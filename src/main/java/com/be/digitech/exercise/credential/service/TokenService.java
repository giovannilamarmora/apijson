package com.be.digitech.exercise.credential.service;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.be.digitech.exercise.credential.dto.TokenDTO;
import com.be.digitech.exercise.credential.dto.UserDTO;
import com.be.digitech.exercise.credential.exception.CredentialException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {

	private static final String ROLE = "role";

	@Value(value = "${jwt.secret}")
	private String secret;

	@Value(value = "${jwt.time}")
	private String expirationTime;

	public TokenDTO generateToken(UserDTO user) {
		Claims claims = Jwts.claims().setSubject(user.getUsername());
		claims.put(ROLE, user.getRole());
		long dateExp = Long.parseLong(expirationTime);
		Date exp = new Date(System.currentTimeMillis() + dateExp);
		String token = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, secret).setExpiration(exp)
				.compact();
		return new TokenDTO(token);
	}

	public UserDTO parseToken(TokenDTO token) throws CredentialException {
		Claims body;
		try {
			body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token.getToken()).getBody();
		} catch (JwtException e) {
			throw new CredentialException(CredentialException.Code.UNAUTHORIZED);
		}
		UserDTO user = new UserDTO(body.getSubject(), (@NotNull String) body.get(ROLE));
		return user;
	}
}
