package com.be.digitech.exercise.credential.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.be.digitech.exercise.credential.dto.TokenDTO;
import com.be.digitech.exercise.credential.dto.UserCredentialDTO;
import com.be.digitech.exercise.credential.dto.UserDTO;
import com.be.digitech.exercise.credential.exception.CredentialException;
import com.be.digitech.exercise.credential.service.UserCredentialService;

@RestController
@RequestMapping("/credential")
public class UserCredentialController {

	@Autowired
	private UserCredentialService service;

	@PostMapping("/signup")
	public ResponseEntity<String> addUser(@RequestBody UserCredentialDTO userCredential) throws CredentialException {
		service.signUp(userCredential);
		return ResponseEntity.status(200).build();
	}

	@PostMapping("/login")
	public TokenDTO loginUser(@RequestBody UserCredentialDTO userCredential) throws CredentialException {
		return service.login(userCredential);
	}

	@GetMapping("/token")
	public UserDTO tokenUser(@RequestHeader(value = "Authorization") String jwt) throws CredentialException {
		TokenDTO token = new TokenDTO(jwt);
		return service.getUser(token);
	}

	@GetMapping("/admin")
	public List<UserDTO> adminListUsers(@RequestHeader(value = "Authorization") String jwt) throws CredentialException {
		TokenDTO token = new TokenDTO(jwt);
		return service.getUsers(token);
	}
}
