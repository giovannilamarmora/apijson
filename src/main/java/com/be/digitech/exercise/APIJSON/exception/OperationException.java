package com.be.digitech.exercise.APIJSON.exception;

public class OperationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7908100412745708723L;
	private final Code code;

	public static enum Code {
		INVALID_NUMBER, ZERO_NUMBERS;
	}

	public OperationException(Code code) {
		super(code.name());
		this.code = code;
	}

	public Code getCode() {
		return code;
	}
}
