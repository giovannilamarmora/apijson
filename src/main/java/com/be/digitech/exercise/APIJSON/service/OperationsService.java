package com.be.digitech.exercise.APIJSON.service;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.stereotype.Service;

import com.be.digitech.exercise.APIJSON.dto.Numbers;
import com.be.digitech.exercise.APIJSON.dto.Result;
import com.be.digitech.exercise.APIJSON.exception.OperationException;

@Service
public class OperationsService {

	public Result sum(Numbers numbers) throws OperationException {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Numbers>> violations = validator.validate(numbers);
		if (!violations.isEmpty()) {
			throw new OperationException(OperationException.Code.INVALID_NUMBER);
		}
		if (numbers.getNumber1() == 0 && numbers.getNumber2() == 0) {
			throw new OperationException(OperationException.Code.ZERO_NUMBERS);
		}

		Result res = new Result(numbers.getNumber1() + numbers.getNumber2());
		return res;
	}

}
