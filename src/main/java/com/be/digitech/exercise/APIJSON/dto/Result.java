package com.be.digitech.exercise.APIJSON.dto;

public class Result {

	private int result;

	public Result(int result) {
		super();
		this.result = result;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
}
