package com.be.digitech.exercise.APIJSON.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.be.digitech.exercise.APIJSON.dto.Numbers;
import com.be.digitech.exercise.APIJSON.dto.Result;
import com.be.digitech.exercise.APIJSON.exception.OperationException;
import com.be.digitech.exercise.APIJSON.service.OperationsService;

@RestController
@RequestMapping("/operation")
public class OperationController {

	@Autowired
	private OperationsService operation;

	@PostMapping("/sum")
	public Result result(@RequestBody Numbers number) throws OperationException {

		return operation.sum(number);

	}

}
