package com.be.digitech.exercise.APIJSON.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.be.digitech.exercise.APIJSON.exception.OperationException;

@ControllerAdvice
public class OperationExceptionController {

	@ExceptionHandler(value = OperationException.class)
	public ResponseEntity<String> exception(OperationException e) {
		e.printStackTrace();
		switch (e.getCode()) {
		case INVALID_NUMBER:
			return new ResponseEntity<>("Error for a Bad Input", HttpStatus.BAD_REQUEST);
		case ZERO_NUMBERS:
			return new ResponseEntity<>("Can't sum Zero Numbers", HttpStatus.CONFLICT);
		default:
			return new ResponseEntity<>("Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
